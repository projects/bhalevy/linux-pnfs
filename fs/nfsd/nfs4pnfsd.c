/******************************************************************************
 *
 * (c) 2007 Network Appliance, Inc.  All Rights Reserved.
 * (c) 2009 NetApp.  All Rights Reserved.
 *
 * NetApp provides this source code under the GPL v2 License.
 * The GPL v2 license is available at
 * http://opensource.org/licenses/gpl-license.php.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

#include "pnfsd.h"
#include "netns.h"

#define NFSDDBG_FACILITY                NFSDDBG_PNFS

/*
 * w.r.t layout lists and recalls, layout_lock protects readers from a writer
 * All modifications to per-file layout state / layout lists are done under the file_lo_lock
 * The only writer-exclusion done with layout_lock is for the sbid table
 */
static DEFINE_SPINLOCK(layout_lock);

#define ASSERT_LAYOUT_LOCKED()	assert_spin_locked(&layout_lock);

/*
 * Layout state - NFSv4.1 pNFS
 */
static struct kmem_cache *pnfs_layout_slab;
static struct kmem_cache *layout_state_slab;
static struct kmem_cache *pnfs_layoutrecall_slab;

/* hash table for nfsd4_pnfs_deviceid.sbid */
#define SBID_HASH_BITS	8
#define SBID_HASH_SIZE	(1 << SBID_HASH_BITS)
#define SBID_HASH_MASK	(SBID_HASH_SIZE - 1)

struct sbid_tracker {
	u64 id;
	struct super_block *sb;
	struct list_head hash;
};

static u64 current_sbid;
static struct list_head sbid_hashtbl[SBID_HASH_SIZE];

static unsigned long
sbid_hashval(struct super_block *sb)
{
	return hash_ptr(sb, SBID_HASH_BITS);
}

static struct sbid_tracker *
alloc_sbid(void)
{
	return kmalloc(sizeof(struct sbid_tracker), GFP_KERNEL);
}

static void
destroy_sbid(struct sbid_tracker *sbid)
{
	spin_lock(&layout_lock);
	list_del(&sbid->hash);
	spin_unlock(&layout_lock);
	kfree(sbid);
}

void
nfsd4_free_pnfs_slabs(void)
{
	int i;
	struct sbid_tracker *sbid;

	nfsd4_free_slab(&pnfs_layout_slab);
	nfsd4_free_slab(&layout_state_slab);
	nfsd4_free_slab(&pnfs_layoutrecall_slab);

	/* statically allocated table hasn't been initialized yet. */
	if (!sbid_hashtbl[0].next)
		return;

	for (i = 0; i < SBID_HASH_SIZE; i++) {
		while (!list_empty(&sbid_hashtbl[i])) {
			sbid = list_first_entry(&sbid_hashtbl[i],
						struct sbid_tracker,
						hash);
			destroy_sbid(sbid);
		}
	}
}

int
nfsd4_init_pnfs_slabs(void)
{
	int i;

	pnfs_layout_slab = kmem_cache_create("pnfs_layouts",
			sizeof(struct nfs4_layout), 0, 0, NULL);
	if (pnfs_layout_slab == NULL)
		return -ENOMEM;
	pnfs_layoutrecall_slab = kmem_cache_create("pnfs_layoutrecalls",
			sizeof(struct nfs4_layoutrecall), 0, 0, NULL);
	if (pnfs_layoutrecall_slab == NULL)
		return -ENOMEM;

	layout_state_slab = kmem_cache_create("pnfs_layout_states",
			sizeof(struct nfs4_layout_state), 0, 0, NULL);
	if (layout_state_slab == NULL)
		return -ENOMEM;

	for (i = 0; i < SBID_HASH_SIZE; i++)
		INIT_LIST_HEAD(&sbid_hashtbl[i]);

	return 0;
}

/*
 * Note: must be called under the state lock
 */
static struct nfs4_layout_state *
alloc_init_layout_state(struct nfs4_client *clp, struct nfs4_file *fp,
			stateid_t *stateid)
{
	struct nfs4_layout_state *new;

	nfs4_assert_state_locked();
	new = layoutstateid(nfsd4_alloc_stid(clp, layout_state_slab));
	if (!new)
		return new;
	kref_init(&new->ls_ref);
	new->ls_stid.sc_type = NFS4_LAYOUT_STID;
	INIT_LIST_HEAD(&new->ls_perclnt);
	INIT_LIST_HEAD(&new->ls_perfile);
	INIT_LIST_HEAD(&new->ls_layouts);
	new->ls_client = clp;
	get_nfs4_file(fp);	/* released on destroy_layout_state */
	new->ls_file = fp;
	new->ls_roc = false;
	spin_lock(&layout_lock);
	list_add(&new->ls_perclnt, &clp->cl_lo_states);
	list_add(&new->ls_perfile, &fp->fi_lo_states);
	spin_unlock(&layout_lock);
	return new;
}

static void
get_layout_state(struct nfs4_layout_state *ls)
{
	kref_get(&ls->ls_ref);
}

static void
unhash_layout_state(struct nfs4_layout_state *ls)
{
	ASSERT_LAYOUT_LOCKED();
	list_del_init(&ls->ls_perclnt);
	list_del_init(&ls->ls_perfile);
}

/*
 * Note: must be called under the state lock
 */
static void
destroy_layout_state(struct kref *kref)
{
	struct nfs4_layout_state *ls =
			container_of(kref, struct nfs4_layout_state, ls_ref);
	struct nfs4_file *fp;

	nfsd4_remove_stid(&ls->ls_stid);
	if (!list_empty(&ls->ls_perclnt)) {
		spin_lock(&layout_lock);
		unhash_layout_state(ls);
		spin_unlock(&layout_lock);
	}
	fp = ls->ls_file;
	nfsd4_free_stid(layout_state_slab, &ls->ls_stid);
	put_nfs4_file_locked(fp);
}

/*
 * Note: must be called under the state lock
 */
static void
put_layout_state(struct nfs4_layout_state *ls)
{
	dprintk("pNFS %s: ls %p ls_ref %d\n", __func__, ls,
		atomic_read(&ls->ls_ref.refcount));
	nfs4_assert_state_locked();
	kref_put(&ls->ls_ref, destroy_layout_state);
}

/*
 * If the layout state was found in cache, grab a reference count on it;
 * otherwise, allocate a new layout state if the client provided a open/
 * lock/deleg stateid.
 *
 * Called with the state_lock held
 * Returns zero and the layout state in *lsp, or error.
 */
static __be32
nfs4_find_create_layout_stateid(struct nfs4_client *clp, struct nfs4_file *fp,
				stateid_t *stateid, unsigned char typemask,
				struct nfs4_layout_state **lsp)
{
	struct nfs4_layout_state *ls = NULL;
	__be32 status;
	struct nfs4_stid *stid;

	dprintk("--> %s clp %p fp %p operation stateid=" STATEID_FMT "\n",
		__func__, clp, fp, STATEID_VAL(stateid));

	nfs4_assert_state_locked();
	status = nfsd4_lookup_stateid(stateid, typemask, &stid, true,
				      net_generic(clp->net, nfsd_net_id));
	if (status)
		goto out;

	status = nfserr_bad_stateid;

	/* Is this the first use of this layout ? */
	switch (stid->sc_type) {
	case NFS4_OPEN_STID:
	case NFS4_LOCK_STID:
		if (openlockstateid(stid)->st_file != fp)
			goto out;
		goto new_stateid;
	case NFS4_DELEG_STID:
		if (delegstateid(stid)->dl_file != fp)
			goto out;
	new_stateid:
		ls = alloc_init_layout_state(clp, fp, stateid);
		if (!ls) {
			status = nfserr_jukebox;
			goto out;
		}
		break;
	case NFS4_LAYOUT_STID:
		ls = container_of(stid, struct nfs4_layout_state, ls_stid);

		if (ls->ls_file != fp ||
		    stateid->si_generation > ls->ls_stid.sc_stateid.si_generation)
			goto out;
		get_layout_state(ls);
		break;
	default:
		goto out;
	}

	status = nfs_ok;

	dprintk("%s: layout stateid=" STATEID_FMT " ref=%d\n", __func__,
		STATEID_VAL(&ls->ls_stid.sc_stateid), atomic_read(&ls->ls_ref.refcount));

	*lsp = ls;
out:
	dprintk("<-- %s status %d\n", __func__, htonl(status));

	return status;
}

/*
 * If the layout state was found in cache, grab a reference count on it.
 *
 * Called with the state_lock held
 * Returns zero and the layout state in *lsp, or error.
 */
static __be32
nfs4_process_layout_stateid(struct nfs4_client *clp,
			    struct nfs4_file *fp,
			    stateid_t *stateid, unsigned char typemask,
			    struct nfs4_layout_state **lsp)
{
	struct nfs4_layout_state *ls = NULL;
	__be32 status;
	struct nfs4_stid *stid;

	dprintk("--> %s clp %p operation stateid=" STATEID_FMT "\n",
		__func__, clp, STATEID_VAL(stateid));

	nfs4_assert_state_locked();
	status = nfsd4_lookup_stateid(stateid, typemask, &stid, true,
				      net_generic(clp->net, nfsd_net_id));
	if (status)
		goto out;

	/* Is this the first use of this layout ? */
	if (stid->sc_type != NFS4_LAYOUT_STID) {
		status = nfserr_bad_stateid;
		goto out;
	}

	ls = container_of(stid, struct nfs4_layout_state, ls_stid);

	if (ls->ls_file != fp ||
	    stateid->si_generation > ls->ls_stid.sc_stateid.si_generation) {
		dprintk("%s bad stateid 1\n", __func__);
		status = nfserr_bad_stateid;
		goto out;
	}
	get_layout_state(ls);

	dprintk("%s: layout stateid=" STATEID_FMT " ref=%d\n", __func__,
		STATEID_VAL(&ls->ls_stid.sc_stateid), atomic_read(&ls->ls_ref.refcount));

	*lsp = ls;
out:
	dprintk("<-- %s status %d\n", __func__, htonl(status));

	return status;
}

static struct nfs4_layout *
alloc_layout(void)
{
	return kmem_cache_alloc(pnfs_layout_slab, GFP_KERNEL);
}

static void
free_layout(struct nfs4_layout *lp)
{
	kmem_cache_free(pnfs_layout_slab, lp);
}

static void update_layout_stateid_locked(struct nfs4_layout_state *ls, stateid_t *sid)
{
	update_stateid(&(ls)->ls_stid.sc_stateid);
	memcpy((sid), &(ls)->ls_stid.sc_stateid, sizeof(stateid_t));
	dprintk("%s Updated ls_stid to %d on layoutstate %p\n",
		__func__, sid->si_generation, ls);
}

static void update_layout_roc(struct nfs4_layout_state *ls, bool roc)
{
	if (roc) {
		ls->ls_roc = true;
		dprintk("%s: Marked return_on_close on layoutstate %p\n",
			__func__, ls);
	}
}

static void
init_layout(struct nfs4_layout *lp,
	    struct nfs4_layout_state *ls,
	    struct svc_fh *current_fh,
	    struct nfsd4_layout_seg *seg,
	    stateid_t *stateid)
{
	dprintk("pNFS %s: lp %p ls %p ino %lu\n", __func__,
		lp, ls, ls->ls_file->fi_inode->i_ino);

	memcpy(&lp->lo_seg, seg, sizeof(lp->lo_seg));
	get_layout_state(ls);		/* put on destroy_layout */
	lp->lo_state = ls;
	spin_lock(&layout_lock);
	update_layout_stateid_locked(ls, stateid);
	list_add_tail(&lp->lo_perstate, &ls->ls_layouts);
	spin_unlock(&layout_lock);
	dprintk("pNFS %s end\n", __func__);
}

/*
 * Note: must be called under the layout_lock.
 */
static void
dequeue_layout_for_return(struct nfs4_layout *lo,
                          struct list_head *lo_destroy_list)
{
	ASSERT_LAYOUT_LOCKED();
	list_del_init(&lo->lo_perstate);
	list_add_tail(&lo->lo_perstate, lo_destroy_list);
	if (list_empty(&lo->lo_state->ls_layouts)) {
		unhash_layout_state(lo->lo_state);
		nfsd4_unhash_stid(&lo->lo_state->ls_stid);
	}
}

/*
 * Note: must be called under the state lock
 */
static void
destroy_layout(struct nfs4_layout *lp)
{
	struct nfs4_layout_state *ls;

	ls = lp->lo_state;
	dprintk("pNFS %s: lp %p ls %p ino %lu\n",
		__func__, lp, ls, ls->ls_file->fi_inode->i_ino);

	free_layout(lp);
	/* release references taken by init_layout */
	put_layout_state(ls);
}

/*
 * Note: must be called under the state lock
 */
static void
destroy_layout_list(struct list_head *lo_destroy_list)
{
	struct nfs4_layout *lp;

	while (!list_empty(lo_destroy_list)) {
		lp = list_first_entry(lo_destroy_list, struct nfs4_layout, lo_perstate);
		list_del(&lp->lo_perstate);
		destroy_layout(lp);
	}
}

static u64
alloc_init_sbid(struct super_block *sb)
{
	struct sbid_tracker *sbid;
	struct sbid_tracker *new = alloc_sbid();
	unsigned long hash_idx = sbid_hashval(sb);
	u64 id = 0;

	if (likely(new)) {
		spin_lock(&layout_lock);
		id = ++current_sbid;
		new->id = (id << SBID_HASH_BITS) | (hash_idx & SBID_HASH_MASK);
		id = new->id;
		BUG_ON(id == 0);
		new->sb = sb;

		list_for_each_entry (sbid, &sbid_hashtbl[hash_idx], hash)
			if (sbid->sb == sb) {
				kfree(new);
				id = sbid->id;
				spin_unlock(&layout_lock);
				return id;
			}
		list_add(&new->hash, &sbid_hashtbl[hash_idx]);
		spin_unlock(&layout_lock);
	}
	return id;
}

struct super_block *
find_sbid_id(u64 id)
{
	struct sbid_tracker *sbid;
	struct super_block *sb = NULL;
	unsigned long hash_idx = id & SBID_HASH_MASK;
	int pos = 0;

	spin_lock(&layout_lock);
	list_for_each_entry (sbid, &sbid_hashtbl[hash_idx], hash) {
		pos++;
		if (sbid->id != id)
			continue;
		if (pos > 1)
			list_move(&sbid->hash, &sbid_hashtbl[hash_idx]);
		sb = sbid->sb;
		break;
	}
	spin_unlock(&layout_lock);
	return sb;
}

u64
find_create_sbid(struct super_block *sb)
{
	struct sbid_tracker *sbid;
	unsigned long hash_idx = sbid_hashval(sb);
	int pos = 0;
	u64 id = 0;

	spin_lock(&layout_lock);
	list_for_each_entry (sbid, &sbid_hashtbl[hash_idx], hash) {
		pos++;
		if (sbid->sb != sb)
			continue;
		if (pos > 1)
			list_move(&sbid->hash, &sbid_hashtbl[hash_idx]);
		id = sbid->id;
		break;
	}
	spin_unlock(&layout_lock);

	if (!id)
		id = alloc_init_sbid(sb);

	return id;
}

/*
 * Create a layoutrecall structure
 * An optional layoutrecall can be cloned (except for the layoutrecall lists)
 */
static struct nfs4_layoutrecall *
alloc_init_layoutrecall(struct nfsd4_pnfs_cb_layout *cbl,
			struct nfs4_file *lrfile)
{
	struct nfs4_layoutrecall *clr;

	dprintk("NFSD %s\n", __func__);
	clr = kmem_cache_alloc(pnfs_layoutrecall_slab, GFP_KERNEL);
	if (clr == NULL)
		return clr;

	dprintk("NFSD %s -->\n", __func__);

	memset(clr, 0, sizeof(*clr));
	if (lrfile)
		get_nfs4_file(lrfile);
	clr->clr_file = lrfile;
	clr->cb = *cbl;

	/* put in nfsd4_cb_layout_release or in layoutrecall_done for parent */
	kref_init(&clr->clr_ref);
	INIT_LIST_HEAD(&clr->clr_perclnt);
	nfsd4_init_callback(&clr->clr_recall);
	clr->done = 0;

	dprintk("NFSD %s return %p\n", __func__, clr);
	return clr;
}

static void
get_layoutrecall(struct nfs4_layoutrecall *clr)
{
	dprintk("pNFS %s: clr %p clr_ref %d\n", __func__, clr,
		atomic_read(&clr->clr_ref.refcount));
	kref_get(&clr->clr_ref);
}

/*
 * Note: caller MUST NOT hold the layout_lock as put_nfs4_file might sleep
 */
static void
destroy_layoutrecall(struct kref *kref)
{
	struct nfs4_layoutrecall *clr =
			container_of(kref, struct nfs4_layoutrecall, clr_ref);
	dprintk("pNFS %s: clr %p fp %p clp %p\n", __func__, clr,
		clr->clr_file, clr->clr_client);
	BUG_ON(!list_empty(&clr->clr_perclnt));
	if (clr->clr_file)
		put_nfs4_file(clr->clr_file);
	kmem_cache_free(pnfs_layoutrecall_slab, clr);
}

int
put_layoutrecall(struct nfs4_layoutrecall *clr)
{
	dprintk("pNFS %s: clr=%p clr_ref=%d done=%u\n", __func__, clr,
		atomic_read(&clr->clr_ref.refcount), clr->done);
	return kref_put(&clr->clr_ref, destroy_layoutrecall);
}

/*
 * Note: must be called under the layout_lock
 * Does not put clr, not clr->parent
 */
static int
layoutrecall_mark_done(const char *func,
                       struct nfs4_layoutrecall *clr,
                       int flag,
                       int cb_status,
                       struct list_head *done_list)
{
	int done;

	dprintk("%s: clr=%p parent=%p clr_ref=%d clr_file=%p "
		"clr->done=%d clr->flag=%d clr->cb_status=%d "
		"flag=%d cb_status=%d\n",
		func, clr, clr->parent,
		atomic_read(&clr->clr_ref.refcount),
		clr->clr_file, clr->done, clr->flag, clr->cb_status,
		flag, cb_status);

	ASSERT_LAYOUT_LOCKED();
	done = clr->done;
	if (!clr->done) {
		clr->done = 1;
		clr->flag = flag;
		clr->cb_status = cb_status;
		list_del_init(&clr->clr_perclnt);

		if (done_list) {
			list_add_tail(&clr->clr_perclnt, done_list);
			get_layoutrecall(clr);	/* put in layoutrecall_list_done_put */
		}
	}

	return done;
}

static void
_layoutrecall_done_put(struct nfs4_layoutrecall *clr)
{
	void *recall_cookie = clr->cb.cbl_cookie;
	struct nfs4_layoutrecall *parent = clr->parent;

	if (parent && !put_layoutrecall(parent))
		recall_cookie = NULL;

	if (recall_cookie) {
		struct nfs4_file *fp = clr->clr_file;
		struct inode *ino = fp->fi_inode;

		nfs4_file_lo_lock(fp);
		ino->i_sb->s_pnfs_op->layout_recall_done(ino, recall_cookie, clr->flag);
		nfs4_file_lo_unlock(fp);
	}

	put_layoutrecall(clr);
}

void
layoutrecall_done(struct nfs4_layoutrecall *clr, int cb_status)
{
	spin_lock(&layout_lock);
	if (layoutrecall_mark_done(__func__, clr, 0, cb_status, NULL)) {
		spin_unlock(&layout_lock);
		dprintk("%s: clr=%p already marked done\n", __func__, clr);
		return;
	}
	spin_unlock(&layout_lock);
	_layoutrecall_done_put(clr);
}

static void
layoutrecall_list_done_put(struct list_head *list)
{
	struct nfs4_layoutrecall *clr;

	while (!list_empty(list)) {
		clr = list_first_entry(list, struct nfs4_layoutrecall, clr_perclnt);
		list_del_init(&clr->clr_perclnt);
		_layoutrecall_done_put(clr);
		put_layoutrecall(clr);
	}
}

/*
 * are two octet ranges overlapping?
 * start1            last1
 *   |-----------------|
 *                start2            last2
 *                  |----------------|
 */
static int
lo_seg_overlapping(struct nfsd4_layout_seg *l1, struct nfsd4_layout_seg *l2)
{
	u64 start1 = l1->offset;
	u64 last1 = last_byte_offset(start1, l1->length);
	u64 start2 = l2->offset;
	u64 last2 = last_byte_offset(start2, l2->length);
	int ret;

	/* if last1 == start2 there's a single byte overlap */
	ret = (last2 >= start1) && (last1 >= start2);
	dprintk("%s: l1 %llu:%lld l2 %llu:%lld ret=%d\n", __func__,
		l1->offset, l1->length, l2->offset, l2->length, ret);
	return ret;
}

static int
same_fsid_major(struct nfs4_fsid *fsid, u64 major)
{
	return fsid->major == major;
}

static int
same_fsid(struct nfs4_fsid *fsid, struct svc_fh *current_fh)
{
	return same_fsid_major(fsid, current_fh->fh_export->ex_fsid);
}

/*
 * find a layout recall conflicting with the specified layoutget
 */
static int
is_layout_recalled(struct nfs4_client *clp,
		   struct nfsd4_pnfs_layoutget *lgp)
{
	struct nfs4_layoutrecall *clr;
	struct nfsd4_layout_seg *seg = &lgp->lg_seg;

	spin_lock(&layout_lock);
	list_for_each_entry (clr, &clp->cl_layoutrecalls, clr_perclnt) {
		if (clr->cb.cbl_seg.layout_type != seg->layout_type)
			continue;
		if (clr->cb.cbl_recall_type == RETURN_ALL)
			goto found;
		if (clr->cb.cbl_recall_type == RETURN_FSID) {
			if (same_fsid(&clr->cb.cbl_fsid, lgp->lg_fhp))
				goto found;
			else
				continue;
		}
		BUG_ON(clr->cb.cbl_recall_type != RETURN_FILE);
		if (same_stid((stateid_t *)&clr->cb.cbl_sid, &lgp->lg_sid) &&
		    lo_seg_overlapping(&clr->cb.cbl_seg, seg))
			goto found;
	}
	spin_unlock(&layout_lock);
	return 0;
found:
	spin_unlock(&layout_lock);
	return 1;
}

/*
 * are two octet ranges overlapping or adjacent?
 */
static bool
lo_seg_mergeable(struct nfsd4_layout_seg *l1, struct nfsd4_layout_seg *l2)
{
	u64 start1 = l1->offset;
	u64 end1 = end_offset(start1, l1->length);
	u64 start2 = l2->offset;
	u64 end2 = end_offset(start2, l2->length);

	/* is end1 == start2 ranges are adjacent */
	return (end2 >= start1) && (end1 >= start2);
}

static void
extend_layout(struct nfsd4_layout_seg *lo, struct nfsd4_layout_seg *lg)
{
	u64 lo_start = lo->offset;
	u64 lo_end = end_offset(lo_start, lo->length);
	u64 lg_start = lg->offset;
	u64 lg_end = end_offset(lg_start, lg->length);

	/* lo already covers lg? */
	if (lo_start <= lg_start && lg_end <= lo_end)
		return;

	/* extend start offset */
	if (lo_start > lg_start)
		lo_start = lg_start;

	/* extend end offset */
	if (lo_end < lg_end)
		lo_end = lg_end;

	lo->offset = lo_start;
	lo->length = (lo_end == NFS4_MAX_UINT64) ?
		      lo_end : lo_end - lo_start;
}

static bool
merge_layout(struct nfs4_layout_state *ls, struct nfsd4_layout_seg *seg)
{
	bool ret = false;
	struct nfs4_layout *lp;

	spin_lock(&layout_lock);
	list_for_each_entry (lp, &ls->ls_layouts, lo_perstate)
		if (lp->lo_seg.layout_type == seg->layout_type &&
		    lp->lo_seg.clientid == seg->clientid &&
		    lp->lo_seg.iomode == seg->iomode &&
		    (ret = lo_seg_mergeable(&lp->lo_seg, seg))) {
			extend_layout(&lp->lo_seg, seg);
			break;
		}
	spin_unlock(&layout_lock);

	return ret;
}

__be32
nfs4_pnfs_get_layout(struct svc_rqst *rqstp,
		     struct nfsd4_pnfs_layoutget *lgp,
		     struct exp_xdr_stream *xdr)
{
	u32 status;
	__be32 nfserr;
	struct inode *ino = lgp->lg_fhp->fh_dentry->d_inode;
	struct super_block *sb = ino->i_sb;
	int can_merge;
	struct nfs4_file *fp;
	struct nfs4_client *clp;
	struct nfs4_layout *lp = NULL;
	struct nfs4_layout_state *ls = NULL;
	struct nfsd4_pnfs_layoutget_arg args = {
		.lg_minlength = lgp->lg_minlength,
		.lg_fh = &lgp->lg_fhp->fh_handle,
	};
	struct nfsd4_pnfs_layoutget_res res = {
		.lg_seg = lgp->lg_seg,
	};

	dprintk("NFSD: %s Begin\n", __func__);

	/* verify minlength and range as per RFC5661:
	 *  o  If loga_length is less than loga_minlength,
	 *     the metadata server MUST return NFS4ERR_INVAL.
	 *  o  If the sum of loga_offset and loga_minlength exceeds
	 *     NFS4_UINT64_MAX, and loga_minlength is not
	 *     NFS4_UINT64_MAX, the error NFS4ERR_INVAL MUST result.
	 *  o  If the sum of loga_offset and loga_length exceeds
	 *     NFS4_UINT64_MAX, and loga_length is not NFS4_UINT64_MAX,
	 *     the error NFS4ERR_INVAL MUST result.
	 */
	if ((lgp->lg_seg.length < lgp->lg_minlength) ||
	    (lgp->lg_minlength != NFS4_MAX_UINT64 &&
	     lgp->lg_minlength > NFS4_MAX_UINT64 - lgp->lg_seg.offset) ||
	    (lgp->lg_seg.length != NFS4_MAX_UINT64 &&
	     lgp->lg_seg.length > NFS4_MAX_UINT64 - lgp->lg_seg.offset)) {
		nfserr = nfserr_inval;
		goto out;
	}

	args.lg_sbid = find_create_sbid(sb);
	if (!args.lg_sbid) {
		nfserr = nfserr_layouttrylater;
		goto out;
	}

	can_merge = sb->s_pnfs_op->can_merge_layouts != NULL &&
		    sb->s_pnfs_op->can_merge_layouts(lgp->lg_seg.layout_type);

	nfs4_lock_state();
	fp = find_alloc_file(ino, lgp->lg_fhp);
	clp = find_confirmed_client((clientid_t *)&lgp->lg_seg.clientid, true,
				    net_generic(SVC_NET(rqstp), nfsd_net_id));
	dprintk("pNFS %s: fp %p clp %p\n", __func__, fp, clp);
	if (!fp || !clp) {
		nfserr = nfserr_inval;
		goto out_unlock;
	}

	/* Check decoded layout stateid */
	nfserr = nfs4_find_create_layout_stateid(clp, fp, &lgp->lg_sid,
						 (NFS4_OPEN_STID | NFS4_LOCK_STID |
						  NFS4_DELEG_STID | NFS4_LAYOUT_STID),
						 &ls);
	if (nfserr)
		goto out_unlock;

	if (is_layout_recalled(clp, lgp)) {
		nfserr = nfserr_recallconflict;
		goto out_unlock;
	}

	nfs4_unlock_state();

	/* pre-alloc layout in case we can't merge after we call
	 * the file system
	 */
	lp = alloc_layout();
	if (!lp) {
		nfserr = nfserr_jukebox;
		goto out_relock;
	}

	dprintk("pNFS %s: pre-export type 0x%x maxcount %Zd "
		"iomode %u offset %llu length %llu\n",
		__func__, lgp->lg_seg.layout_type,
		exp_xdr_qbytes(xdr->end - xdr->p),
		lgp->lg_seg.iomode, lgp->lg_seg.offset, lgp->lg_seg.length);

	nfs4_file_lo_lock(fp);

	status = sb->s_pnfs_op->layout_get(ino, xdr, &args, &res);

	dprintk("pNFS %s: post-export status %u "
		"iomode %u offset %llu length %llu\n",
		__func__, status, res.lg_seg.iomode,
		res.lg_seg.offset, res.lg_seg.length);

	/*
	 * The allowable error codes for the layout_get pNFS export
	 * operations vector function (from the file system) can be
	 * expanded as needed to include other errors defined for
	 * the RFC 5561 LAYOUTGET operation.
	 */
	switch (status) {
	case 0:
		nfserr = NFS4_OK;
		break;
	case NFS4ERR_ACCESS:
	case NFS4ERR_BADIOMODE:
		/* No support for LAYOUTIOMODE4_RW layouts */
	case NFS4ERR_BADLAYOUT:
		/* No layout matching loga_minlength rules */
	case NFS4ERR_INVAL:
	case NFS4ERR_IO:
	case NFS4ERR_LAYOUTTRYLATER:
	case NFS4ERR_LAYOUTUNAVAILABLE:
	case NFS4ERR_LOCKED:
	case NFS4ERR_NOSPC:
	case NFS4ERR_RECALLCONFLICT:
	case NFS4ERR_SERVERFAULT:
	case NFS4ERR_TOOSMALL:
		/* Requested layout too big for loga_maxcount */
	case NFS4ERR_WRONG_TYPE:
		/* Not a regular file */
		nfserr = cpu_to_be32(status);
		goto out_freelayout;
	default:
		BUG();
		nfserr = nfserr_serverfault;
	}

	lgp->lg_seg = res.lg_seg;
	lgp->lg_roc = res.lg_return_on_close;
	update_layout_roc(ls, res.lg_return_on_close);

	/* SUCCESS!
	 * Can the new layout be merged into an existing one?
	 * If so, free unused layout struct
	 */
	if (can_merge && merge_layout(ls, &res.lg_seg))
		goto out_freelayout;

	/* Can't merge, so let's initialize this new layout */
	init_layout(lp, ls, lgp->lg_fhp, &res.lg_seg, &lgp->lg_sid);

out_unlock_file:
	nfs4_file_lo_unlock(fp);
out_relock:
	nfs4_lock_state();	/* required for put_layout_state */
out_unlock:
	if (ls)
		put_layout_state(ls);
	nfs4_unlock_state();
	if (fp)
		put_nfs4_file(fp);
out:
	dprintk("pNFS %s: lp %p exit nfserr %u\n", __func__, lp,
		be32_to_cpu(nfserr));
	return nfserr;

out_freelayout:
	free_layout(lp);
	goto out_unlock_file;
}

__be32
nfs4_pnfs_commit_layout(struct svc_rqst *rqstp, struct svc_fh *current_fh,
			struct nfsd4_pnfs_layoutcommit *lcp)
{
	struct inode *ino = current_fh->fh_dentry->d_inode;
	struct super_block *sb = ino->i_sb;
	struct nfs4_file *fp;
	struct nfs4_client *clp;
	struct nfs4_layout_state *ls = NULL;
	__be32 nfserr = NFS4_OK;
	int status = 0;

	lcp->res.lc_size_chg = 0;

	nfs4_lock_state();
	fp = find_file(ino);
	clp = find_confirmed_client((clientid_t *)&lcp->lc_clientid,
				    true, net_generic(SVC_NET(rqstp), nfsd_net_id));
	if (!fp || !clp) {
		nfserr = nfserr_inval;
		nfs4_unlock_state();
		goto out;
	}

	nfserr = nfs4_process_layout_stateid(clp, fp, &lcp->lc_sid,
					     NFS4_LAYOUT_STID, &ls);
	nfs4_unlock_state();
	if (nfserr) {
		/* fixup error code as per RFC5661 */
		if (nfserr == nfserr_bad_stateid)
			nfserr = nfserr_badlayout;
		goto out;
	}

	if (sb->s_pnfs_op->layout_commit) {
		nfs4_file_lo_lock(fp);
		status = sb->s_pnfs_op->layout_commit(ino, &lcp->args, &lcp->res);
		nfs4_file_lo_unlock(fp);
		if (status) {
			nfserr = nfserrno(status);
			dprintk("%s:layout_commit result %u\n", __func__, status);
			goto out;
		}
	} else if (lcp->args.lc_newoffset) {
		loff_t newsize = lcp->args.lc_last_wr + 1;

		if (newsize <= ino->i_size)
			goto out;

		fh_lock(current_fh);
		if (newsize <= ino->i_size) {
			fh_unlock(current_fh);
			goto out;
		}

		lcp->res.lc_size_chg = 1;
		lcp->res.lc_newsize = newsize;
		dprintk("%s: Modifying file size\n", __func__);
		i_size_write(ino, newsize);
		mark_inode_dirty(ino);
		if (EX_ISSYNC(current_fh->fh_export))
			status = write_inode_now(ino, 1);
		fh_unlock(current_fh);
		if (status) {
			dprintk("%s:write_inode_now result %d\n", __func__, status);
			nfserr = nfserrno(status);
			goto out;
		}
	}

out:
	if (fp || ls) {
		nfs4_lock_state();
		if (fp)
			put_nfs4_file_locked(fp);
		if (ls)
			put_layout_state(ls);
		nfs4_unlock_state();
	}

	return nfserr;
}

static void
trim_layout(struct nfsd4_layout_seg *lo, struct nfsd4_layout_seg *lr)
{
	u64 lo_start = lo->offset;
	u64 lo_end = end_offset(lo_start, lo->length);
	u64 lr_start = lr->offset;
	u64 lr_end = end_offset(lr_start, lr->length);

	dprintk("%s:Begin lo %llu:%lld lr %llu:%lld\n", __func__,
		lo->offset, lo->length, lr->offset, lr->length);

	/* lr fully covers lo? */
	if (lr_start <= lo_start && lo_end <= lr_end) {
		lo->length = 0;
		goto out;
	}

	/*
	 * split not supported yet. retain layout segment.
	 * remains must be returned by the client
	 * on the final layout return.
	 */
	if (lo_start < lr_start && lr_end < lo_end) {
		dprintk("%s: split not supported\n", __func__);
		goto out;
	}

	if (lo_start < lr_start)
		lo_end = lr_start - 1;
	else /* lr_end < lo_end */
		lo_start = lr_end + 1;

	lo->offset = lo_start;
	lo->length = (lo_end == NFS4_MAX_UINT64) ? lo_end : lo_end - lo_start;
out:
	dprintk("%s:End lo %llu:%lld\n", __func__, lo->offset, lo->length);
}

/*
 * Note: must be called under file_lo_lock ONLY.
 *
 * The layout_lock may not be held here.
 *
 * The state_lock may not be held here and it is the caller responsibility
 * to destroy the layout returned here.
 */
static void
return_layout_to_fs(struct nfs4_layout *lo, int lr_flags)
{
	struct inode *ino = lo->lo_state->ls_file->fi_inode;
	struct super_block *sb = ino->i_sb;
	struct nfsd4_pnfs_layoutreturn lr;
	int ret;

	if (unlikely(!sb->s_pnfs_op->layout_return))
		return;

	memset(&lr, 0, sizeof(lr));
	lr.args.lr_return_type = RETURN_FILE;
	lr.args.lr_seg = lo->lo_seg;
	lr.args.lr_flags = lr_flags;

	ret = sb->s_pnfs_op->layout_return(ino, &lr.args);
	dprintk("%s: inode %lu iomode=%d offset=0x%llx length=0x%llx "
		"flags=0x%x status=%d\n",
		__func__, ino->i_ino, lr.args.lr_seg.iomode,
		lr.args.lr_seg.offset, lr.args.lr_seg.length,
		lr_flags, ret);
}

/*
 * Note: should be called WITHOUT holding the state_lock nor the layout_lock
 */
static int
pnfs_return_file_layouts(struct nfsd4_pnfs_layoutreturn *lrp,
			 struct nfs4_layout_state *ls,
			 int lr_flags,
			 struct list_head *lo_destroy_list)
{
	int layouts_found = 0;
	struct nfs4_layout *lp, *nextlp;

	nfs4_file_lo_lock(ls->ls_file);

	dprintk("%s: ls %p\n", __func__, ls);
	lrp->lrs_present = 0;
	spin_lock(&layout_lock);
	list_for_each_entry_safe (lp, nextlp, &ls->ls_layouts, lo_perstate) {
		dprintk("%s: lp %p ls %p inode %lu lo_type %x,%x iomode %d,%d\n",
			__func__, lp, lp->lo_state,
			lp->lo_state->ls_file->fi_inode->i_ino,
			lp->lo_seg.layout_type, lrp->args.lr_seg.layout_type,
			lp->lo_seg.iomode, lrp->args.lr_seg.iomode);
		if ((lp->lo_seg.layout_type != lrp->args.lr_seg.layout_type &&
		     lrp->args.lr_seg.layout_type) ||
		    (lp->lo_seg.iomode != lrp->args.lr_seg.iomode &&
		     lrp->args.lr_seg.iomode != IOMODE_ANY) ||
		     !lo_seg_overlapping(&lp->lo_seg, &lrp->args.lr_seg)) {
			lrp->lrs_present = 1;
			continue;
		}
		layouts_found++;
		trim_layout(&lp->lo_seg, &lrp->args.lr_seg);
		if (!lp->lo_seg.length) {
			dequeue_layout_for_return(lp, lo_destroy_list);
			spin_unlock(&layout_lock);

			return_layout_to_fs(lp, lr_flags);

			spin_lock(&layout_lock);
		} else
			lrp->lrs_present = 1;
	}
	if (ls && layouts_found && lrp->lrs_present)
		update_layout_stateid_locked(ls, (stateid_t *)&lrp->args.lr_sid);
	spin_unlock(&layout_lock);

	nfs4_file_lo_unlock(ls->ls_file);

	return layouts_found;
}

/*
 * Return layouts for RETURN_FSID or RETURN_ALL
 *
 * Note: must be called WITHOUT the state lock nor the layout lock
 */
static int
pnfs_return_client_layouts(struct nfs4_client *clp,
			   struct nfsd4_pnfs_layoutreturn *lrp,
			   u64 ex_fsid, int lr_flags,
			   struct list_head *lo_destroy_list)
{
	int layouts_found = 0;
	bool state_found;
	struct nfs4_layout_state *ls, *nextls;
	struct nfs4_layout *lp, *nextlp;

	spin_lock(&layout_lock);
	list_for_each_entry_safe (ls, nextls, &clp->cl_lo_states, ls_perclnt) {
		if (lrp->args.lr_return_type == RETURN_FSID &&
		    !same_fsid_major(&ls->ls_file->fi_fsid, ex_fsid))
			continue;

		/* first pass, test only */
		state_found = false;
		list_for_each_entry (lp, &ls->ls_layouts, lo_perstate) {
			if (lrp->args.lr_seg.layout_type != lp->lo_seg.layout_type &&
			    lrp->args.lr_seg.layout_type)
				break;

			if (lrp->args.lr_seg.iomode != lp->lo_seg.iomode &&
			    lrp->args.lr_seg.iomode != IOMODE_ANY)
				continue;

			state_found = true;
			break;
		}

		if (!state_found)
			continue;

		get_layout_state(ls);
		spin_unlock(&layout_lock);

		/* second pass, actually return the layouts */
		nfs4_file_lo_lock(ls->ls_file);
		spin_lock(&layout_lock);
		list_for_each_entry_safe (lp, nextlp, &ls->ls_layouts, lo_perstate) {
			if (lrp->args.lr_seg.layout_type != lp->lo_seg.layout_type &&
			    lrp->args.lr_seg.layout_type)
				break;

			if (lrp->args.lr_seg.iomode != lp->lo_seg.iomode &&
			    lrp->args.lr_seg.iomode != IOMODE_ANY)
				continue;

			layouts_found++;
			dequeue_layout_for_return(lp, lo_destroy_list);
			spin_unlock(&layout_lock);

			return_layout_to_fs(lp, lr_flags);

			spin_lock(&layout_lock);
		}
		spin_unlock(&layout_lock);
		nfs4_file_lo_unlock(ls->ls_file);

		nfs4_lock_state();
		put_layout_state(ls);
		nfs4_unlock_state();

		spin_lock(&layout_lock);
	}
	spin_unlock(&layout_lock);
	return layouts_found;
}

static int
recall_return_perfect_match(struct nfs4_layoutrecall *clr,
			    struct nfsd4_pnfs_layoutreturn *lrp,
			    struct nfs4_file *fp,
			    struct svc_fh *current_fh)
{
	if (clr->cb.cbl_seg.iomode != lrp->args.lr_seg.iomode ||
	    clr->cb.cbl_recall_type != lrp->args.lr_return_type)
		return 0;

	return (clr->cb.cbl_recall_type == RETURN_FILE &&
		clr->clr_file == fp &&
		clr->cb.cbl_seg.offset == lrp->args.lr_seg.offset &&
		clr->cb.cbl_seg.length == lrp->args.lr_seg.length) ||

		(clr->cb.cbl_recall_type == RETURN_FSID &&
		 same_fsid(&clr->cb.cbl_fsid, current_fh)) ||

		clr->cb.cbl_recall_type == RETURN_ALL;
}

static int
recall_return_partial_match(struct nfs4_layoutrecall *clr,
			    struct nfsd4_pnfs_layoutreturn *lrp,
			    struct nfs4_file *fp,
			    struct svc_fh *current_fh)
{
	/* iomode matching? */
	if (clr->cb.cbl_seg.iomode != lrp->args.lr_seg.iomode &&
	    clr->cb.cbl_seg.iomode != IOMODE_ANY &&
	    lrp->args.lr_seg.iomode != IOMODE_ANY)
		return 0;

	if (clr->cb.cbl_recall_type == RETURN_ALL ||
	    lrp->args.lr_return_type == RETURN_ALL)
		return 1;

	/* fsid matches? */
	if (clr->cb.cbl_recall_type == RETURN_FSID ||
	    lrp->args.lr_return_type == RETURN_FSID)
		return same_fsid(&clr->cb.cbl_fsid, current_fh);

	/* file matches, range overlapping? */
	return clr->clr_file == fp &&
	       lo_seg_overlapping(&clr->cb.cbl_seg, &lrp->args.lr_seg);
}

int nfs4_pnfs_return_layout(struct svc_rqst *rqstp,
			    struct super_block *sb,
			    struct svc_fh *current_fh,
			    struct nfsd4_pnfs_layoutreturn *lrp)
{
	int status = 0;
	int layouts_found = 0;
	struct inode *ino = current_fh->fh_dentry->d_inode;
	struct nfs4_file *fp = NULL;
	struct nfs4_layout_state *ls = NULL;
	struct nfs4_client *clp;
	struct nfs4_layoutrecall *clr, *nextclr;
	u64 ex_fsid = current_fh->fh_export->ex_fsid;
	LIST_HEAD(lo_destroy_list);

	dprintk("NFSD: %s\n", __func__);

	nfs4_lock_state();
	clp = find_confirmed_client(&lrp->lr_clientid,
				    true, net_generic(SVC_NET(rqstp), nfsd_net_id));
	if (!clp)
		goto out_unlock;

	if (lrp->args.lr_return_type == RETURN_FILE) {
		fp = find_file(ino);
		if (!fp) {
			dprintk("%s: RETURN_FILE: no nfs4_file for ino %p:%lu\n",
				__func__, ino, ino ? ino->i_ino : 0L);
			/* If we had a layout on the file the nfs4_file would
			 * be referenced and we should have found it. Since we
			 * don't then it means all layouts were ROC and at this
			 * point we returned all of them on file close.
			 */
			goto out_unlock;
		}

		/* Check the stateid */
		dprintk("%s PROCESS LO_STATEID inode %p\n", __func__, ino);
		status = nfs4_process_layout_stateid(clp, fp,
						     (stateid_t *)&lrp->args.lr_sid,
						     NFS4_LAYOUT_STID, &ls);
		nfs4_unlock_state();
		if (status)
			goto out;
		layouts_found = pnfs_return_file_layouts(lrp, ls,
		                                         0, &lo_destroy_list);
	} else {
		nfs4_unlock_state();
		layouts_found = pnfs_return_client_layouts(clp, lrp, ex_fsid,
		                                           0, &lo_destroy_list);
	}

	dprintk("pNFS %s: clp %p fp %p ino %lu layout_type 0x%x iomode %d "
		"return_type %d fsid 0x%llx offset %llu length %llu: "
		"layouts_found %d\n",
		__func__, clp, fp, ino->i_ino, lrp->args.lr_seg.layout_type,
		lrp->args.lr_seg.iomode, lrp->args.lr_return_type,
		ex_fsid,
		lrp->args.lr_seg.offset, lrp->args.lr_seg.length, layouts_found);

	/* update layoutrecalls
	 * note: for RETURN_{FSID,ALL}, fp may be NULL
	 */
	spin_lock(&layout_lock);
	list_for_each_entry_safe (clr, nextclr, &clp->cl_layoutrecalls,
				  clr_perclnt) {
		if (clr->cb.cbl_seg.layout_type != lrp->args.lr_seg.layout_type)
			continue;

		if (recall_return_perfect_match(clr, lrp, fp, current_fh))
			layoutrecall_mark_done(__func__, clr, 0, 0, &lo_destroy_list);
		else if (layouts_found &&
			 recall_return_partial_match(clr, lrp, fp, current_fh))
			clr->clr_time = CURRENT_TIME;
	}
	spin_unlock(&layout_lock);

	layoutrecall_list_done_put(&lo_destroy_list);

	/* put_layout_state here as we need to keep a reference on ls_file */
	nfs4_lock_state();
	if (fp)
		put_nfs4_file_locked(fp);
	if (ls)
		put_layout_state(ls);
	destroy_layout_list(&lo_destroy_list);
	nfs4_unlock_state();

out:
	dprintk("pNFS %s: exit status %d\n", __func__, status);
	return status;
out_unlock:
	nfs4_unlock_state();
	goto out;
}

/*
 * Note: must be called WITHOUT holding the state lock
 */
void
nomatching_layout(struct nfs4_layoutrecall *clr, int lr_flags)
{
	struct nfsd4_pnfs_layoutreturn lr = {
		.args.lr_return_type = clr->cb.cbl_recall_type,
		.args.lr_seg = clr->cb.cbl_seg,
	};
	LIST_HEAD(lo_destroy_list);

	dprintk("%s: clp %p fp %p: simulating layout_return\n", __func__,
		clr->clr_client, clr->clr_file);

	if (clr->cb.cbl_recall_type == RETURN_FILE) {
		struct nfs4_layout_state *ls;
		bool found = false;

		spin_lock(&layout_lock);
		list_for_each_entry (ls, &clr->clr_client->cl_lo_states, ls_perclnt)
			if (ls->ls_file == clr->clr_file) {
				get_layout_state(ls);
				spin_unlock(&layout_lock);

				found = true;
				pnfs_return_file_layouts(&lr, ls, lr_flags,
				                         &lo_destroy_list);
				break;
			}

		if (found) {
			nfs4_lock_state();
			put_layout_state(ls);
			destroy_layout_list(&lo_destroy_list);
			nfs4_unlock_state();
		} else
			spin_unlock(&layout_lock);
	} else {
		pnfs_return_client_layouts(clr->clr_client, &lr,
		                           clr->cb.cbl_fsid.major, lr_flags,
		                           &lo_destroy_list);
	}
}

/*
 * Note: must be called under the state lock
 */
void pnfs_expire_client(struct nfs4_client *clp)
{
	struct nfsd4_pnfs_layoutreturn lr = {
		.args.lr_return_type = RETURN_ALL,
		.args.lr_seg = {
			.iomode = IOMODE_ANY,
			.offset = 0,
			.length = NFS4_MAX_UINT64,
		},
	};
	LIST_HEAD(lo_destroy_list);

	nfs4_assert_state_locked();
	nfs4_unlock_state();

	for (;;) {
		struct nfs4_layoutrecall *lrp = NULL;

		spin_lock(&layout_lock);
		if (!list_empty(&clp->cl_layoutrecalls)) {
			lrp = list_first_entry(&clp->cl_layoutrecalls,
					 struct nfs4_layoutrecall, clr_perclnt);
			if (WARN_ON(lrp->done)) {	/* not supposed to be on the list */
				list_del_init(&lrp->clr_perclnt);
				spin_unlock(&layout_lock);
				continue;
			}
			layoutrecall_mark_done(__func__, lrp, LR_FLAG_EXPIRE, 0, NULL);
		}
		spin_unlock(&layout_lock);
		if (!lrp)
			break;

		dprintk("%s: lrp %p, fp %p\n", __func__, lrp, lrp->clr_file);
		BUG_ON(lrp->clr_client != clp);
		nomatching_layout(lrp, LR_FLAG_EXPIRE);
		_layoutrecall_done_put(lrp);
	}

	pnfs_return_client_layouts(clp, &lr, 0, LR_FLAG_EXPIRE, &lo_destroy_list);

	nfs4_lock_state();
	destroy_layout_list(&lo_destroy_list);
}

/* Return On Close:
 *   Look for all layouts of @fp that belong to @clp, remove
 *   the layout and simulate a layout_return. Surly the client has forgotten
 *   these layouts or it would return them before the close.
 *
 * Note: must be called under the state lock
 */
void pnfsd_roc(struct nfs4_client *clp, struct nfs4_file *fp)
{
	struct nfsd4_pnfs_layoutreturn lr = {
		.args.lr_return_type = RETURN_FILE,
		.args.lr_seg = {
			.iomode = IOMODE_ANY,
			.offset = 0,
			.length = NFS4_MAX_UINT64,
		},
	};
	LIST_HEAD(lo_destroy_list);
	LIST_HEAD(clr_done_list);
	struct nfs4_layout_state *ls;
	struct nfs4_layoutrecall *clr, *nextclr;

	nfs4_assert_state_locked();
	nfs4_unlock_state();

	spin_lock(&layout_lock);
	list_for_each_entry (ls, &fp->fi_lo_states, ls_perfile) {
		if (ls->ls_client != clp)
			continue;
		spin_unlock(&layout_lock);
		pnfs_return_file_layouts(&lr, ls, LR_FLAG_INTERN, &lo_destroy_list);
		spin_lock(&layout_lock);
		break;
	}

	list_for_each_entry_safe (clr, nextclr, &clp->cl_layoutrecalls, clr_perclnt)
		if (clr->clr_file == fp)
			layoutrecall_mark_done(__func__, clr, LR_FLAG_INTERN,
			                       0, &clr_done_list);
	spin_unlock(&layout_lock);

	layoutrecall_list_done_put(&clr_done_list);

	nfs4_lock_state();
	destroy_layout_list(&lo_destroy_list);
}

/*
 * Note: must be called under the layout lock
 */
static struct nfs4_layout_state *
should_recall_file_layout(struct nfsd4_pnfs_cb_layout *cbl,
			  struct nfs4_file *fp)
{
	struct nfs4_layout_state *ls, *ret = NULL;
	stateid_t *stid = (stateid_t *)&cbl->cbl_sid;
	struct nfs4_layout *lo;

	dprintk("%s: ino=%lu stateid=" STATEID_FMT " iomode=%u", __func__,
		fp->fi_inode->i_ino, STATEID_VAL(stid),
		cbl->cbl_seg.iomode);

	list_for_each_entry (ls, &fp->fi_lo_states, ls_perfile) {
		if (!is_null_stid(stid) &&
		    !same_stid(stid, &ls->ls_stid.sc_stateid))
			continue;

		list_for_each_entry (lo, &ls->ls_layouts, lo_perstate)
			if (cbl->cbl_seg.layout_type == lo->lo_seg.layout_type &&
			    lo_seg_overlapping(&cbl->cbl_seg, &lo->lo_seg) &&
			    (cbl->cbl_seg.iomode & lo->lo_seg.iomode)) {
				ret = ls;
				break;
			}
	}

	return ret;
}

/*
 * Create a layoutrecall structure for each client based on the
 * original structure.
 */
int
create_layout_recall_list(struct list_head *todolist, unsigned *todo_len,
			  struct nfsd4_pnfs_cb_layout *cbl,
			  struct nfs4_file *lrfile)
{
	struct nfs4_layout_state *ls;
	struct nfs4_layoutrecall *pending;
	int status = 0;

	dprintk("%s: -->\n", __func__);

	/* We do not support wildcard recalls yet */
	if (cbl->cbl_recall_type != RETURN_FILE)
		return -EOPNOTSUPP;

	/* Matching put done by layoutreturn */
	pending = alloc_init_layoutrecall(cbl, lrfile);
	if (!pending)
		return -ENOMEM;

	switch (cbl->cbl_recall_type) {
	case RETURN_FILE:
		spin_lock(&layout_lock);
		ls = should_recall_file_layout(cbl, lrfile);
		if (ls) {
			update_layout_stateid_locked(ls,
					(stateid_t *)&pending->cb.cbl_sid);
			pending->clr_client = ls->ls_client;
			list_add(&pending->clr_perclnt, todolist);
			(*todo_len)++;
		} else
			put_layoutrecall(pending);
		spin_unlock(&layout_lock);
		break;
	case RETURN_FSID:
	default:
		WARN_ON(1);
		put_layoutrecall(pending);
		return -EINVAL;	/* not supported yet */
	}

	dprintk("%s: <-- list len %u status %d\n", __func__, *todo_len, status);
	return status;
}

/*
 * Recall layouts asynchronously
 * Called with state lock.
 */
static int
spawn_layout_recall(struct super_block *sb, struct list_head *todolist,
		    unsigned todo_len)
{
	struct nfs4_layoutrecall *pending;
	struct nfs4_layoutrecall *parent = NULL;
	int status = 0;

	dprintk("%s: -->\n", __func__);

	if (todo_len > 1) {
		pending = list_entry(todolist->next, struct nfs4_layoutrecall,
				     clr_perclnt);

		parent = alloc_init_layoutrecall(&pending->cb,
						 pending->clr_file);
		if (unlikely(!parent)) {
			/* We want forward progress. If parent cannot be
			 * allocated take the first one as parent but don't
			 * execute it.  Caller must check for -EAGAIN, if so
			 * When the partial recalls return,
			 * nfsd_layout_recall_cb should be called again.
			 */
			list_del_init(&pending->clr_perclnt);
			if (todo_len > 2) {
				parent = pending;
			} else {
				parent = NULL;
				put_layoutrecall(pending);
			}
			--todo_len;
			status = -ENOMEM;
		}
	}

	while (!list_empty(todolist)) {
		pending = list_entry(todolist->next, struct nfs4_layoutrecall,
				     clr_perclnt);
		list_del_init(&pending->clr_perclnt);
		dprintk("%s: clp %p cb_client %p fp %p\n", __func__,
			pending->clr_client,
			pending->clr_client->cl_cb_client,
			pending->clr_file);
		if (unlikely(!pending->clr_client->cl_cb_client)) {
			printk(KERN_INFO
				"%s: clientid %08x/%08x has no callback path\n",
				__func__,
				pending->clr_client->cl_clientid.cl_boot,
				pending->clr_client->cl_clientid.cl_id);
			put_layoutrecall(pending);
			continue;
		}

		pending->clr_time = CURRENT_TIME;
		pending->clr_sb = sb;
		if (parent) {
			/* If we created a parent its initial ref count is 1.
			 * We will need to de-ref it eventually. So we just
			 * don't increment on behalf of the last one.
			 */
			if (todo_len != 1)
				get_layoutrecall(parent);
		}
		pending->parent = parent;
		get_layoutrecall(pending);	/* put in layoutrecall_done */
		/* Add to list so corresponding layoutreturn can find req */
		spin_lock(&layout_lock);
		list_add(&pending->clr_perclnt,
			 &pending->clr_client->cl_layoutrecalls);
		spin_unlock(&layout_lock);

		nfsd4_cb_layout(pending);
		--todo_len;
	}

	return status;
}

/*
 * Spawn a thread to perform a recall layout
 *
 */
int nfsd_layout_recall_cb(struct super_block *sb, struct inode *inode,
			  struct nfsd4_pnfs_cb_layout *cbl)
{
	int status;
	struct nfs4_file *lrfile = NULL;
	struct list_head todolist;
	unsigned todo_len = 0;

	dprintk("NFSD nfsd_layout_recall_cb: inode %p cbl %p\n", inode, cbl);
	BUG_ON(!cbl);
	BUG_ON(cbl->cbl_recall_type != RETURN_FILE &&
	       cbl->cbl_recall_type != RETURN_FSID &&
	       cbl->cbl_recall_type != RETURN_ALL);
	BUG_ON(cbl->cbl_recall_type == RETURN_FILE && !inode);
	BUG_ON(cbl->cbl_seg.iomode != IOMODE_READ &&
	       cbl->cbl_seg.iomode != IOMODE_RW &&
	       cbl->cbl_seg.iomode != IOMODE_ANY);

	status = -ENOENT;
	if (inode) {
		lrfile = find_file(inode);
		if (!lrfile) {
			dprintk("NFSD nfsd_layout_recall_cb: "
				"nfs4_file not found\n");
			goto err;
		}
		if (cbl->cbl_recall_type == RETURN_FSID)
			cbl->cbl_fsid = lrfile->fi_fsid;
	}

	INIT_LIST_HEAD(&todolist);

	status = create_layout_recall_list(&todolist, &todo_len, cbl, lrfile);
	if (list_empty(&todolist)) {
		status = -ENOENT;
	} else {
		/* process todolist even if create_layout_recall_list
		 * returned an error */
		int status2 = spawn_layout_recall(sb, &todolist, todo_len);
		if (status2)
			status = status2;
	}

err:
	if (lrfile)
		put_nfs4_file(lrfile);
	if (status && todo_len)
		status = -EAGAIN;
	dprintk("%s: status=%d\n", __func__, status);
	return status;
}

int
nfsd_layout_iomode_cb(struct inode *inode)
{
	struct nfs4_file *fp;
	struct nfs4_layout_state *ls;
	struct nfs4_layout *lo;
	u32 ret = 0;

	fp = find_file(inode);
	if (!fp)
		return -EINVAL;

	spin_lock(&layout_lock);
	list_for_each_entry(ls, &fp->fi_lo_states, ls_perfile)
		list_for_each_entry(lo, &ls->ls_layouts, lo_perstate) {
			ret |= lo->lo_seg.iomode;
			if (ret == (IOMODE_READ | IOMODE_RW))
				break;
		}
	spin_unlock(&layout_lock);
	put_nfs4_file(fp);

	return ret;
}
